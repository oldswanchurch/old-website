---
title: "Sermons"
date: 2021-02-14
---

Sermons are hosted by [Anchor.fm](https://anchor.fm/oldswanchurch) and can be listened to on most podcast providers (although currently not Amazon Music).

<center>
<iframe src="https://anchor.fm/oldswanchurch/embed" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>
</center>

