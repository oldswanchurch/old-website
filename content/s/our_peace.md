---
title: "He Is Our Peace"
date: 2020-11-29T13:03:50Z
draft: false
text: "Ephesians 2"
preacher: "Sam Tomlin"
---

## Message

<iframe width="560" height="315" src="https://www.youtube.com/embed/nVxKzbgesrU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
