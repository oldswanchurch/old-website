---
title: "Heaven"
date: 2020-02-23T13:01:28Z
draft: false
text: "Revelation 21-22"
preacher: "Arthur"
---

## Passage
[Revelation 21-22](https://www.biblegateway.com/passage/?search=Revelation21-22&version=NIV)

## Audio
[Listen to the sermon recording](/audio/2020-02-23-am--Revelation22--Heaven.m4a).
