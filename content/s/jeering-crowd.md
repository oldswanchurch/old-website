---
title: "The Jeering Crowd"
date: 2020-03-15T15:59:35Z
draft: false
text: "Mark 15:16-32"
preacher: "Lynn Bruce"
---

## Passage
[Mark 15:16-32](https://www.biblegateway.com/passage/?search=Mark+15%3A16-32&version=NIV)

> The soldiers led Jesus away into the palace (that is, the Praetorium) and called together the whole company of soldiers. They put a purple robe on him, then twisted together a crown of thorns and set it on him. And they began to call out to him, “Hail, king of the Jews!” Again and again they struck him on the head with a staff and spit on him. Falling on their knees, they paid homage to him. And when they had mocked him, they took off the purple robe and put his own clothes on him. Then they led him out to crucify him.
>
> A certain man from Cyrene, Simon, the father of Alexander and Rufus, was passing by on his way in from the country, and they forced him to carry the cross. They brought Jesus to the place called Golgotha (which means “the place of the skull”). Then they offered him wine mixed with myrrh, but he did not take it. And they crucified him. Dividing up his clothes, they cast lots to see what each would get.
>
> It was nine in the morning when they crucified him. The written notice of the charge against him read: the king of the jews.
>
> They crucified two rebels with him, one on his right and one on his left. [a] Those who passed by hurled insults at him, shaking their heads and saying, “So! You who are going to destroy the temple and build it in three days, come down from the cross and save yourself!” In the same way the chief priests and the teachers of the law mocked him among themselves. “He saved others,” they said, “but he can’t save himself! Let this Messiah, this king of Israel, come down now from the cross, that we may see and believe.” Those crucified with him also heaped insults on him.

## Audio
[Listen to the sermon recording](/audio/2020-03-15-am--Mark15--JeeringCrowds.m4a).

## Slides
<iframe src="/slides/2020-03-20--The Jeering Crowd.pdf" style="border:0px #ffffff none;" scrolling="no" frameborder="1" marginheight="0px" marginwidth="0px" height="500px" width="600px" allowfullscreen></iframe>

## Notes

The start of a short series looking at people who are more peripheral to the story of Easter

I did Simon of Cyrene several years ago which was fascinating

Palm Sunday: 6 days before Passover. Jesus rides into Jerusalem on a donkey. The adoring crowd wave palm branches and greet him as their king. 
These were mainly relatively local people. People who’d followed Jesus throughout his ministry, even if they hadn’t seen Jesus in person, they probably knew quite a bit about him.

At Passover the population of Jerusalem swelled massively
Compulsory for every adult male Jew who lived within 15 miles of Jerusalem
It was the one ambition of all Jews to eat at least one Passover in Jerusalem before they died so they came from every country in the world
The Passover feast lasted 7 days in total and all lodging was free
There were people lodging in the villages of Bethany and Bethphage, and probably camping on the hillsides because Jerusalem could not cope with the crowds

Eating the Passover lamb was part of the Passover meal – a sacrifice of thanksgiving for freedom from slavery, freedom for liberation

The historian Josephus looked at a census of lambs killed and they numbered 250,000 – the rules stated that you needed a minimum of 10 people per lamb = over 2 million people. It seems incredible, I don’t know how accurate that is. Anyhow, there were a LOT of people

There was a festival atmosphere and the massive crowd were basically looking for anything that would keep them occupied, amuse them.

This crowd was a DIFFERENT crowd to the Palm Sunday crowd. It’s unlikely they had heard Jesus teach, but they had certainly heard a great many rumours about him – some right, some wrong

Was he the Messiah – basically it didn’t look like that now. 
Many others who had claimed to be new Jewish leaders who would overthrow the Romans had also been executed

He was fair game as a bit of entertainment – as was anyone else who was being crucified that day.

The crowd were taunting Jesus with something they believed he had said

Word of mouth, no beautifully presented leather-bound edition of New Testament to read then!

Jesus didn’t say HE was going to destroy the temple, but what he could do IF the temple was destroyed

From John’s Gospel, when people asked for a sign that he was the Messiah he replied: 

John 2: 19-22
Jesus answered them, “Destroy this temple, and I will raise it again in three days.”
They replied, “It has taken forty-six years to build this temple, and you are going to raise it in three days?” But the temple he had spoken of was his body. After he was raised from the dead, his disciples recalled what he had said. Then they believed the scripture and the words that Jesus had spoken.

The interesting thing is that the crowd uses the word SAVE…

“come down from the cross and save yourself!”

You can almost imagine the chant building up – Save yourself! Save yourself! They were a mob and people behave differently in mobs

To the crowd Jesus was not a success. 
They lived in hard times and they scorned and taunted him because in the dog-eat-dog world they lived in, you certainly didn’t align yourself with the losing side

They whole crowd at that moment was thinking that first and foremost in this life you save yourself

Today even in our ‘civilized’ society we have a world of individuals who think the only worthwhile thing is me, me, me
A world where I am on every photo I ever take, where MY opinions are the most important thing, where MY success is the only thing that is important

But that isn’t what Jesus taught…

The Jewish leaders keep up the theme of SAVING

They are still looking for signs and wonders

But they give something important away…

Despite engineering the death of Jesus (in their own mini mob) they showed they weren’t completely blind to what he had been doing

In the middle of the scorn they heap on Jesus they acknowledge that Jesus SAVED OTHERS

Jesus gave new life wherever he went. Among others:
He gave a bunch of fishermen a much wider vision of what life is really about
He save Matthew and Zacheus from the shady dishonest way they exploited people when they were collecting taxes
He saved many people from demon possession and illness
He healed Peter’s mother in law from fever
He healed a leper from leprosy, people with physical disabilities
He saved the daughter of Jairus from an early death (she did die eventually…)
He healed many people from things like blindness, epilepsy and deafness

Those people Jesus saved knew the freedom and liberation Jesus came to bring. Freedom to live a new life with a whole different mindset

And controversially for the religious leaders, he FORGAVE people and freed them from their past lives – giving them a new focus for life

SAVING was the key theme of Jesus ministry, along with what people were being saved FOR

The Bible accounts constantly talk about how Jesus was coming to SAVE people:

The angel said to Joseph:
She will give birth to a son, and you are to give him the name Jesus, because he will save his people from their sins. Matthew 1:21

Jesus repeatedly said:
The Son of Man has come to seek and save the lost e.g. Luke 19:10

The Gospel writers and Paul repeatedly say:
The Christian message is the ‘way of salvation’ Acts 16:17

So back to the religious leaders He saved others, but he can’t save himself! 

The religious leaders acknowledge that Jesus saved others – whether they meant physically or spiritually is unclear, but they nevertheless acknowledge that people’s lives changed after meeting Jesus

But even more poignantly, they said he saved OTHERS
NOT them.
They are used to being INSIDERS, the people ‘in the know’, the people who know how to observe complicated rules and please God

But they hadn’t understood the message of Jesus - and they are very much OUTSIDERS at the crucifixion.

Jesus prayed for them because despite supposedly being the ones ‘in the know’, they did not know what they were doing
On the cross Jesus said: Forgive them, because they don’t know what they are doing

Despite all their knowledge and training, the people who heard the teaching of Jesus and really GOT it, were ahead of religious leaders in being part of the Kingdom of God

Just days before he was crucified, when Jesus was explaining to his 12 closest disciples what he had come to do, he says (John 12:24)

“The hour has come for the Son of Man to be glorified. 
Very truly I tell you, unless a kernel of wheat falls to the ground and dies, it remains only a single seed. But if it dies, it produces many seeds.”

You can eat wheat and have the instant benefit of a stomach full of food.

But if you have wheat to spare, you can save it until the next growing season, plant it, tend it, water it until hopefully have a good crop which is many times more than the single seed you planted

You can delay the pleasure of eating that grain to have a harvest which is much bigger


We don’t understand the full mystery of the Crucifixion or how it saved people – that’s one of the things I hope will become much clearer when I meet Jesus face to face!

Somehow, Jesus dying on the cross follows the natural pattern of the earth. Death and resurrection – wheat grains don’t look full of life until you plant them. In fact they look completely dead, and that is one of the everyday miracles of life as we know it

But we do know that in some way the death and resurrection of Jesus was way more fruitful than if he had continued to live and die in the same way as anyone else

There were thousands of people who were crucified by the Romans, and there were many Jewish revolutionaries who wanted to lead the fight against the Roman occupiers who were killed in other ways as well. We know the names of very few of them

But the Resurrection makes Jesus unique

But Jesus says that everyday miracle of death and resurrection also applies to his followers:

Whoever does not take up their cross and follow me is not worthy of me. Whoever finds their life will lose it, and whoever loses their life for my sake will find it

As a young Christian I always thought that this meant that I had to make the ‘ultimate’ sacrifice and die for my faith.

But Jesus is talking much broader than that.

Just like the earliest disciples we are still called to leave behind all the garbage that messes up our lives
All those wrong decisions we make all the time that mess up our lives and the lives of others
All that ambition that says that the most important person in the photo is myself
Everything that says ‘success’ in this world involves trampling on other people, looking for quick personal gain, only caring about ‘me’

The religious leaders hadn’t grasped this
That a real relationship with God wasn’t based on how well they could observe the law, and make other peoples’ lives hell in the process

My own personal view is that Jesus didn’t come to save us from GOD
He came to save us from OURSELVES

To show us a new way of living that was free from condemnation

He sacrifice was celebrating freedom for captives
The freedom from slavery to all the bad stuff in our lives

Yes – when we have done something wrong there are often CONSEQUENCES that have to be put right – people we have to say ‘sorry’ to, situations we have to deal with

But the message of Jesus is that each one of us is free to be the person that God intended us to be all along

If we don’t accept God’s forgiveness, if we keep mulling over past bad stuff in our minds, agonizing over it, then that is a form of ‘me-ness’ too, a self-indulgence that stops us actively being part of God’s Kingdom

Jesus said he’d come to save us (John 10:10): 

I have come that they may have life and have it to the full

What Jesus offers is life in all its fullness

But everyone has to make a decision to leave behind the ‘me’ life and live to follow the teachings of Jesus

All of us feel a long way away a lot of the time
But Jesus isn’t about constantly beating ourselves us
It’s about accepting that we can leave the garbage behind without looking back

So we have a life focus that isn’t about ‘me’, a life that is part of God’s Kingdom in the here and now

**I have come that they may have life, and have it to the full**
