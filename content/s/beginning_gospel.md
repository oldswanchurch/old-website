---
title: "The Beginning of the Gospel"
date: 2021-01-10T13:03:50Z
draft: false
text: "Mark 1"
preacher: "Dan Prior"
---

## Message

<iframe width="560" height="315" src="https://www.youtube.com/embed/yn2MvZ_Yb10" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
