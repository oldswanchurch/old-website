"""Simple program to generate markdown file for new sermon."""

# for getting file paths correct regardless of operating system
import os

# start
opening_message = """
Hello! Let's create a new sermon file.

First I'll need some details...
"""

print(opening_message)

def get_details():
    "get sermon details"

    sermon_title = input('Sermon title?\n')
    month = input('Month as TWO DIGIT NUMBER, eg. September as 09\n')
    date = input('Date as TWO DIGIT NUMBER, eg. first as 01\n')
    sermon_text = input('Bible passage?\n')
    preacher = input('Preacher?\n')
    youtube_code = input("""
Enter the sermon recording YouTube code. 
(The final string of letters and numbers at 
the end of the URL, like 'yn2MvZ_Yb10'\n
""")
    words = input('Summarise the sermon title in two words for a filename\n')

    simple_filename = ''
    words = words.lower().split(' ')
    for word in words:
        simple_filename += f'_{word}'

    # generate markdown file
    sermon_md = f"""
---
title: '{sermon_title}' 
date: 2021-{month}-{date}T13:03:50Z
draft: false
text: '{sermon_text}'
preacher: '{preacher}'
---

## Message

<iframe width="560" height="315" src="https://www.youtube.com/embed/{youtube_code}" allowfullscreen></iframe>
    """

    return sermon_md, simple_filename

def make_sermon_file(sermon_md, simple_filename):

    # write file
    with open(os.path.join('content','sermons',f'{simple_filename}.md'), 'w') as f:
        f.write(sermon_md)

    # finish
    closing_message_1 = """
    Thanks! That should be done now!

    Run `hugo serve` in another terminal window (Powershell/Bash/...) and check it's running at `localhost:1313`.

    """

    print(closing_message_1)


sermon_md, simple_filename = get_details()

done = make_sermon_file(sermon_md, simple_filename)

